#!/usr/bin/python

# Importamos las librerias necesarias
from include.Driver import Driver
from selenium.webdriver.common.by import By
from selenium.webdriver import ActionChains

from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.ui import WebDriverWait

import time
import urllib.request
from os import listdir, getcwd
from os.path import isdir, join


def test():
    '''
    Optimizamos el tamaño y peso de las imagenes
    '''
    # Variables de configuración
    size = '1600' # Nuevo tamaño para la imagen
    quality = '80' # Calidad de la imagen

    # Creamos una instancia del driver
    driver = Driver()

    # Obtenemos el driver
    # d = driver.getDriverFirefox(False)
    d = driver.getDriverChrome(False)
    # d = driver.getDriverIE()

    # Obtenemos la siguiente url
    d.get('http://webresizer.com/resizer/')

    # Esperamos 5 segundos
    d.implicitly_wait(5)

    # //////////////////////////////////////////////
    # Obtenemos los archivos
    # //////////////////////////////////////////////

    # ruta de las imagenes
    dir_path = join(getcwd(), 'images', 'origin')
    print('Obteniendo las imagenes del directorio')

    # Obtenemos los archivos
    for filename in listdir(dir_path):
        # creamos la ruta de la imagen
        path = join(dir_path, filename)
        print('* Subiendo imagen '+ str(filename))

        # Cargamos la imagen
        file_upload = WebDriverWait(d, 10).until(
            ec.presence_of_element_located((By.ID, 'filename'))
        )
        file_upload.send_keys(path)

        # subimos la imagen
        btn = d.find_element(By.ID, 'i4')
        if btn is not None:
           btn.click()

        time.sleep(5)

        # Agregamos el nuevo tamaño
        txt_size = d.find_element(By.ID, 'newSize')
        if txt_size is not None:
            txt_size.clear()
            txt_size.send_keys(size)

        # Agregamos la calidad
        txt_quality = d.find_element(By.ID, 'quality')
        if txt_quality is not None:
            txt_quality.clear()
            txt_quality.send_keys(quality)

        # Aplicamos los cambios
        btn = d.find_element(By.ID, 'i5')
        if btn is not None:
           btn.click()
        print('* Optimizando imagen '+str(filename))

        time.sleep(5)

        # Guardamos la imagen
        new_path = join(getcwd(), 'images', 'dest', filename)

        img = d.find_element(By.ID, 'i078')
        src = img.get_attribute('src')
        urllib.request.urlretrieve(src, new_path)
        print('- Descargando imagen '+str(filename))


    # //////////////////////////////////////////////
    # Finalizamos la sesión
    # //////////////////////////////////////////////

    # cerramos el driver
    d.quit()


if __name__ == "__main__":
    test()
