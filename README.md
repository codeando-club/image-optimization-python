# Optimizaciòn de imagenes con python

Ejemplos y casos de uso para subir archivos utilizando selenium

## Instrucciones

### Dependencias necesarias

* Python 3.
* Selenium.
* PyAutoIt.

### Sobre este repositorio

Clona el repositorio.-

```
git clone https://gitlab.com/codeando-club/image-optimization-python.git
```

Inicializamos el git flow.-

```
git flow init
```

Para iniciar un feature.-

```
git flow feature start MYFEATURE
```

Para finalizar un feature.-

```
git flow feature finish MYFEATURE
```

[Más información](https://danielkummer.github.io/git-flow-cheatsheet/)

### Preparar entorno de desarrollo

Instalar las siguientes dependencias.-

```
pip install selenium PyAutoIt
```

## Instrucciones de uso

En el directorio principal del proyecto encontraras un directorio images y dentro otros dos subdirectorios.

* images
	* origin
	* dest

En el directorio "origin" se deben colocar las imagenes originales, una vez terminado el proceso, en el directorio "dest", encontrarán las imagenes optimizadas.

En el proyecto encontrarn dos archivos.-

* Test.py - Optimiza las imagenes utilizando selenium.
* Test2.py - Optimiza las imagenes utilizando PILLOW.

### Fe de erratas

Si tienes dudas o mejoras en el código, por favor [crea una Issue](https://gitlab.com/codeando-club/image-optimization-python.git/issues).
