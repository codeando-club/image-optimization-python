#!/usr/bin/python

# Importamos las librerias necesarias
from PIL import Image
from os import listdir, getcwd
from os.path import isdir, join


def test():
    '''
    Optimizamos el tamaño y peso de las imagenes
    '''

    # //////////////////////////////////////////////
    # Obtenemos los archivos
    # //////////////////////////////////////////////

    # ruta de las imagenes
    dir_path = join(getcwd(), 'images', 'origin')
    print('Obteniendo las imagenes del directorio')

    # Obtenemos los archivos
    for filename in listdir(dir_path):
        # creamos la ruta de la imagen
        path = join(dir_path, filename)
        
        # Cargamos la imagen
        img = Image.open(path)

        # Guardamos la imagen
        new_path = join(getcwd(), 'images', 'dest', filename)
        img.save(new_path, optimize=True, quality=95)


if __name__ == "__main__":
    test()
