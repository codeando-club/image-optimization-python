#!/usr/bin/python

# importamos la libreria
from selenium import webdriver
from selenium.webdriver.firefox.options import Options

#Creamos el driver para trabajar con selenium
class Driver:
    '''
    Crea un driver para selenium con las opciones personalizadas segun el
    navegador que se utilice
    '''
    # Constuctor
    def __init__(self):
        # Driver paths
        self.path_chromedriver = './drivers/chromedriver'
        self.path_geckodriver = './drivers/geckodriver'
        self.path_iedriver = './drivers/IEDriverServer'
        # Opciones para Firefox
        self.options = Options()
        self.options.add_argument('--ignore-certificate-error')
        # Opciones para Chrome
        self.chrome_options = webdriver.ChromeOptions()
        self.chrome_options.add_argument('--ignore-certificate-error')
        # Opciones para IE
        self.ie_options = webdriver.IeOptions()
        self.ie_options.add_argument('--ignore-certificate-error')
        # https://sqa.stackexchange.com/questions/9904/how-to-set-browser-locale-with-chromedriver-python
        # options.add_experimental_option('prefs', {'intl.accept_languages': 'en,en_US'})
        # Logs
        self.service_args_chrome = ['--verbose', '--log-path=./logs/chromedriver.log']
        self.service_args_gecko = ['--verbose', '--log-path=./logs/geckodriver.log']
        self.service_args_ie = ['--verbose', '--log-path=./logs/iedriver.log']


    # Driver para Firefox
    def getDriverFirefox(self, view = True):
        if not view:
            # Ocultamos el navegador
            self.options.headless = True


        # Creamos el driver
        driver = webdriver.Firefox(executable_path=self.path_geckodriver, options=self.options)

        return driver


    # Driver para Chrome
    def getDriverChrome(self, view = True):
        if not view:
            # Ocultamos el navegador
            self.chrome_options.headless = True
            # Opciones para no mostrar herramientas de Chrome
            self.chrome_options.add_argument("--no-sandbox") # Corremos como root
            self.chrome_options.add_argument("--no-default-browser-check") #Overrides default choices
            self.chrome_options.add_argument("--no-first-run")
            self.chrome_options.add_argument("--disable-default-apps")
            # Opcion solo para windows
            self.chrome_options.add_argument("--disable-gpu")


        # Creamos el driver
        driver = webdriver.Chrome(executable_path=self.path_chromedriver, chrome_options=self.chrome_options)

        return driver


    # Driver para IE
    def getDriverIE(self):
        # Creamos el driver
        driver = webdriver.Ie(executable_path=self.path_iedriver, ie_options=self.ie_options)

        return driver
